import * as firebase from 'firebase';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  LOGOUT,
  SET_TOKEN,
  SIGNIN,
  SIGNUP,
  TRY_SIGNIN,
  TRY_SIGNUP,
  TrySignin,
  TrySignup
} from './auth.actions';
import { map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {
  authSignup = createEffect(() => this.actions$.pipe(
    ofType(TRY_SIGNUP),
    map((action: TrySignup) => action.payload),
    switchMap((authData: {username: string, password: string}) =>
      fromPromise(firebase.auth().createUserWithEmailAndPassword(authData.username, authData.password))),
    switchMap(() => fromPromise(firebase.auth().currentUser.getIdToken())),
    mergeMap((token: string) => {
      return [
        {
          type: SIGNUP
        },
        {
          type: SET_TOKEN,
          payload: token,
        }
      ];
    })
  ));

  singin = createEffect(() => this.actions$.pipe(
    ofType(TRY_SIGNIN),
    map((action: TrySignin) => action.payload),
    switchMap((authData: {username: string, password: string}) =>
      fromPromise(firebase.auth().signInWithEmailAndPassword(authData.username, authData.password))),
    switchMap(() => fromPromise(firebase.auth().currentUser.getIdToken())),
    mergeMap((token: string) => {
      this.router.navigate(['/']);
      return [
        {
          type: SIGNIN
        },
        {
          type: SET_TOKEN,
          payload: token,
        }
      ];
    })
  ));

  authLogout = createEffect(() => this.actions$.pipe(
    ofType(LOGOUT),
    tap(() => this.router.navigate(['/']))
    ), { dispatch: false });
  constructor(private actions$: Actions,
              private router: Router) {
  }
}
