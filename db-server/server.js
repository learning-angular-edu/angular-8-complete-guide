const express = require('express');
const bodyParser = require('body-parser');
const mongoClient = require('mongodb').MongoClient;
//const ObjectId = require('mongodb').ObjectID;
const cors = require('cors');
const CONNECTION_URL = 'mongodb://localhost:27017/edu_db';
const DATABASE_NAME = 'edu_db';

const server = express();

const corsOptions = {
  origing: 'http://localhost:4200'
};

server.use(cors(corsOptions));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));
let database, collection, appCollection, recipesCollection;

server.get("/", (req, res) => {
  res.json({ message: "Welcome to edu application." });
});
server.post('/servers', (request, response) => {
  collection.insert(request.body, (error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    response.send(result.result);
  });
});
server.get('/servers', (request, response) => {
  collection.find({}).toArray((error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    response.send(result);
  });
});
server.get('/appName', (request, response) => {
  appCollection.find({}).toArray((error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    response.send(result[0]);
  });
});

const recipeBookRouter = express.Router({mergeParams: true});
server.use('/ng-recipe-book', recipeBookRouter);
recipeBookRouter.route('/recipes')
  .get((request, response) => {
    recipesCollection.find({}).toArray((error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      response.send(result);
    });
  })
  .post((request, response) => {
    recipesCollection.deleteMany({})
      .then(() => recipesCollection.insertMany(request.body))
      .then(result => response.send(result))
      .catch(error => response.status(500).send(error));
  });

// set port, listen for requests
const PORT = process.env.PORT || 8080;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
  const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true
  };
  mongoClient.connect(CONNECTION_URL, options, (error, client) => {
    if (error) {
      console.log('Cannot connect to database:', error);
      throw error;
    }
    database = client.db(DATABASE_NAME);
    collection = database.collection('servers');
    appCollection = database.collection('appName');
    recipesCollection = database.collection('recipes');
    console.log('Connected to database:', DATABASE_NAME);
  });
});
