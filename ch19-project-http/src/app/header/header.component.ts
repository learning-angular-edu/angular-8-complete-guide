import { Component } from '@angular/core';
import { DataStorageService } from '../shared/data-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.css']
})
export class HeaderComponent {
  constructor(private dataStorageService: DataStorageService) {
  }
  onSaveDate() {
    this.dataStorageService.storeRecipes()
      .subscribe(
        response => console.log('data saved', response),
        err => console.log('data save failed:', err)
      );
  }

  onFetchData() {
    this.dataStorageService.getRecipes();
  }
}
