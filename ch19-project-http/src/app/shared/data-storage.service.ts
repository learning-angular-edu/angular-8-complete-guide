import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RecipeService } from '../recipes/recipe.service';
import { Recipe } from '../recipes/recipe.model';
import { map } from 'rxjs/operators';

@Injectable()
export class DataStorageService {
  constructor(private http: HttpClient,
              private recipeService: RecipeService) {
  }

  storeRecipes() {
    return this.http.post('http://localhost:8080/ng-recipe-book/recipes', this.recipeService.getRecipes());
  }

  getRecipes() {
    this.http.get('http://localhost:8080/ng-recipe-book/recipes').pipe(
      map((recipes: Recipe[]) => recipes.map(recipe => Object.assign({ingredients: []}, recipe)))
    ).subscribe((recipes: Recipe[]) => this.recipeService.setRecipes(recipes));
  }
}
