import { Recipe } from '../recipe.model';
import { Ingredient } from '../../shared/ingredient.model';
import {
  ADD_RECIPE,
  DELETE_RECIPE,
  RecipeActions,
  SET_RECIPES,
  UPDATE_RECIPE
} from './recipe.actions';
import { AppState } from '../../store/app.reducers';

export interface FeatureState extends AppState {
  recipes: RecipeState;
}

export interface RecipeState {
  recipes: Recipe[];
}

const initialState: RecipeState = {
  recipes: [
    new Recipe(
      'Tasty Schnitzel',
      'A super-tasty Schnitzel - just awesome!',
      'https://upload.wikimedia.org/wikipedia/commons/7/72/Schnitzel.JPG',
      [
        new Ingredient('Meat', 1),
        new Ingredient('French Fries', 20)
      ]),
    new Recipe('Big Fat Burger',
      'What else you need to say?',
      'https://upload.wikimedia.org/wikipedia/commons/b/be/Burger_King_Angus_Bacon_%26_Cheese_Steak_Burger.jpg',
      [
        new Ingredient('Buns', 2),
        new Ingredient('Meat', 1)
      ])
  ]
};

export function recipeReducer(state = initialState, action: RecipeActions) {
  switch (action.type) {
    case SET_RECIPES:
      return {
        ...state,
        recipes: [...action.payload],
      };
    case ADD_RECIPE:
      return {
        ...state,
        recipes: [...state.recipes, action.payload],
      };
    case UPDATE_RECIPE:
      const updatedRecipes = [...state.recipes];
      updatedRecipes[action.payload.index] = {
        ...state.recipes[action.payload.index],
        ...action.payload.updatedRecipe
      };
      return {
        ...state,
        recipes: updatedRecipes,
      };
    case DELETE_RECIPE:
      return {
        ...state,
        recipes: [
          ...state.recipes.slice(0, action.payload),
          ...state.recipes.slice(action.payload + 1)
        ],
      };
    default:
      return state;
  }
}
