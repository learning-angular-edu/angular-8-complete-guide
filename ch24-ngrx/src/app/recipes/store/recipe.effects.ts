import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { FETCH_RECIPES, SET_RECIPES, STORE_RECIPES } from './recipe.actions';
import { Recipe } from '../recipe.model';
import { FeatureState } from './recipe.reducers';

@Injectable()
export class RecipeEffects {
  @Effect()
  recipeFetch = this.actions$.pipe(
    ofType(FETCH_RECIPES),
    switchMap(() =>
      this.httpClient.get<Recipe[]>('https://edu-recipes-book.firebaseio.com/recipes.json')),
    map((recipes: Recipe[]) => {
      return {
        type: SET_RECIPES,
        payload: recipes.map(recipe => Object.assign({ ingredients: [] }, recipe))
      };
    })
  );

  @Effect({ dispatch: false })
  recipeStore = this.actions$.pipe(
    ofType(STORE_RECIPES),
    withLatestFrom(this.store.select('recipes')),
    switchMap(([_, state]) => {
      const req = new HttpRequest(
        'PUT',
        'https://edu-recipes-book.firebaseio.com/recipes.json',
        state.recipes,
        {
          reportProgress: true
        });
      return this.httpClient.request(req);
    })
  );

  constructor(private actions$: Actions,
              private httpClient: HttpClient,
              private store: Store<FeatureState>) {
  }
}
