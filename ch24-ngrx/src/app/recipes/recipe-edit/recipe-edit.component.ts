import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { FeatureState, RecipeState } from '../store/recipe.reducers';
import { AddRecipe, UpdateRecipe } from '../store/recipe.actions';
import { take } from 'rxjs/operators';

// import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id: number;
  editMode = false;
  recipeForm: FormGroup;
  readonly ingredientsMin = 1;

  constructor(private route: ActivatedRoute,
              private fb: FormBuilder,
              private router: Router,
              private store: Store<FeatureState>) {
  }

  ngOnInit(): void {
    this.route.params
      .subscribe((params: Params) => {
        this.id = +params.id;
        this.editMode = params.id != null;
        this.initForm();
      });
  }

  get recipeIngredients() {
    return this.recipeForm.get('ingredients') as FormArray;
  }

  private initForm() {
    let recipeName = '';
    let imagePath = '';
    let recipeDescription = '';
    let ingredients = this.fb.array([]);
    if (this.editMode) {
      this.store.select('recipes').pipe(
        take(1)
      ).subscribe((recipeState: RecipeState) => {
        const recipe = recipeState.recipes[this.id];
        recipeName = recipe.name;
        imagePath = recipe.imagePath;
        recipeDescription = recipe.description;
        if (!!recipe.ingredients && recipe.ingredients.length > 0) {
          ingredients = this.fb.array([
            ...recipe.ingredients.map(i => this.ingredientControl(i.name, i.amount))
          ]);
        }
      });
    }
    this.recipeForm = this.fb.group({
      name: [recipeName, Validators.required],
      imagePath: [imagePath, Validators.required],
      description: [recipeDescription, Validators.required],
      ingredients
    });
  }

  private ingredientControl(name, amount) {
    return this.fb.group({
      name: [name, Validators.required],
      amount: [amount, [Validators.required, Validators.min(this.ingredientsMin)]]
    });
  }

  onSubmit() {
    if (this.editMode) {
      this.store.dispatch(new UpdateRecipe({
        index: this.id,
        updatedRecipe: this.recipeForm.value
      }));
    } else {
      this.store.dispatch(new AddRecipe(this.recipeForm.value));
    }
    this.onCancel();
  }

  onAddIngredient() {
    this.recipeIngredients.push(
      this.ingredientControl('', '')
    );
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  onDeleteIngredient(index: number) {
    this.recipeIngredients.removeAt(index);
  }
}
