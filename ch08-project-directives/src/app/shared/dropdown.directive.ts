import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  private isShow = false;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  @HostListener('click') toggleOpen() {
    this.isShow = !this.isShow;
    this.renderDropdownMenuShow();
  }

  private renderDropdownMenuShow() {
    const child = this.elementRef.nativeElement.children[1];
    if (this.isShow) {
      this.renderer.addClass(child, 'show');
    } else {
      this.renderer.removeClass(child, 'show');
    }
  }
}
