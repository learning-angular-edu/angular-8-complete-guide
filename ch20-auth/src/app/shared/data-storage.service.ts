import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { RecipeService } from '../recipes/recipe.service';
import { Recipe } from '../recipes/recipe.model';
import { map } from 'rxjs/operators';

@Injectable()
export class DataStorageService {
  constructor(private httpClient: HttpClient,
              private recipeService: RecipeService) {
  }

  storeRecipes() {
    const req = new HttpRequest(
      'PUT',
      'https://edu-recipes-book.firebaseio.com/recipes.json',
      this.recipeService.getRecipes(),
      {
        reportProgress: true
      });
    return this.httpClient.request(req);
    // return this.httpClient
    //   .put('https://edu-recipes-book.firebaseio.com/recipes.json',
    //     this.recipeService.getRecipes(),
    //     {
    //       params: new HttpParams().set('auth', token)
    //     });
  }

  getRecipes() {
    this.httpClient.get<Recipe[]>('https://edu-recipes-book.firebaseio.com/recipes.json').pipe(
      map((response: Recipe[]) => response.map(recipe => Object.assign({ingredients: []}, recipe)))
    ).subscribe((recipes: Recipe[]) => this.recipeService.setRecipes(recipes));
  }
}
