import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  numbersObsSubscription: Subscription;
  customObsSubscription: Subscription;

  constructor() { }

  ngOnInit(): void {
    const myNumbers = interval(1000).pipe(
      map(data => data * 2),
      filter(data => data % 3 !== 0)
    );
    this.numbersObsSubscription = myNumbers.subscribe(
      (num: number) => {
        console.log(num);
      }
    );
    const myObservable = new Observable(subscriber => {
      setTimeout(() => {
        subscriber.next('first package');
      }, 2000);
      setTimeout(() => {
        subscriber.next('second package');
      }, 4000);
      setTimeout(() => {
        // subscriber.error('this does not work');
        subscriber.complete();
      }, 5000);
      setTimeout(() => {
        subscriber.next('third package');
      }, 6000);
    });
    this.customObsSubscription = myObservable.subscribe(
      data => console.log(data),
      err => console.log(err),
      () => console.log('completed')
    );
  }

  ngOnDestroy(): void {
    this.numbersObsSubscription.unsubscribe();
    this.customObsSubscription.unsubscribe();
  }

}
