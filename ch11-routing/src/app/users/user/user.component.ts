import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: {id: number, name: string};

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.updateUser(this.route.snapshot.params.id, this.route.snapshot.params.name);
    this.route.params.subscribe(
      (params: Params) => this.updateUser(params.id, params.name)
    );
  }

  private updateUser(id: number, name: string) {
    this.user = {
      id,
      name
    };
  }
}
