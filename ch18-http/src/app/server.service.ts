import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class ServerService {
  constructor(private http: HttpClient) {
  }

  storeServers(servers: any[]) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post('http://localhost:8080/servers', servers,
      { headers });
  }

  getServers() {
    return this.http.get('http://localhost:8080/servers').pipe(
      map(
        (response: any[]) => response.map(i => {
          return {
            id: i.id,
            name: i.name,
            capacity: i.capacity
          };
        })
      ),
      catchError(err => {
//        console.log(err);
        return throwError('Something went wrong');
      })
    );
  }

  getAppName() {
    return this.http.get('http://localhost:8080/appName').pipe(
      map((response: any) => response.name)
    );
  }
}
